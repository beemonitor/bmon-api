#!/bin/sh
mkdir -p files/changes
mkdir -p src/main/resources/config
cp config/* src/main/resources/config/
mvn package -Dmaven.test.skip=true
ln -sf target/server.jar server.jar