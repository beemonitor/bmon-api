<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Триггер после добавления новых записей в таблицу пользователей.'
);

$scope->writeAfterWF(
    \helpers\Trigger::beforeEntityChangesTrigger(
        \tables\ApiariesAccess::getInstance(),
        \tables\ApiariesAccessChanges::getInstance()
    ) .
    PHP_EOL . PHP_EOL . PHP_EOL .
    \helpers\Trigger::afterEntityChangesTrigger(
        \tables\ApiariesAccess::getInstance(),
        \tables\ApiariesAccessChanges::getInstance()
    )
);