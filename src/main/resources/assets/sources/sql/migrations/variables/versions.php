<?php
namespace variables;

class Versions
{
    const V0_0_1 = 'v0.0.1';
    const V0_0_2 = 'v0.0.2';
    const V0_0_3 = 'v0.0.3';
    const V0_0_4 = 'v0.0.4';
}