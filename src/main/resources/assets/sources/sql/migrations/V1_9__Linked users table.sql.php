<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица связанных друг с другом пользователей.'
);

$scope->table(\tables\LinkedUsers::getInstance());