<?php

namespace helpers;


use tables\ParametersBigint;
use tables\Storages;

class Trigger
{
    public static function beforeEntityChangesTrigger($entityTable, $entityChangesTable)
    {
        $entityColName = $entityTable->getExternalIdName();
        $seqName = Sequence::getSequenceName($entityTable);
        $declaration = <<<SQL
SQL;
        //TODO: При создании: Если есть таблица сущностей - добавляем в неё и берём ID. Если нет - берём ID из последовательности.
        $assignGlobalParamsQuery = '';
        if ($entityTable->tableExists) {
            $entityTableName = $entityTable->name;
            $entityParams = $entityTable->entityParams;
            $entityParamsStr = '';
            $newParamsStr = '';
            $i = 0;
            foreach ($entityParams as $col) {
                if ($i > 0) {
                    $entityParamsStr .= ',';
                    $newParamsStr .= ',';
                }
                $i++;
                $entityParamsStr .= $col->name;
                $newParamsStr .= 'NEW.' . $col->name;
            }
            $createGlobalIdQuery = <<<SQL
INSERT INTO {$entityTableName} ({$entityParamsStr}) VALUES ({$newParamsStr}) RETURNING id INTO NEW.{$entityColName};
SQL;
        } else {
            $createGlobalIdQuery = "NEW.{$entityColName} = (SELECT nextval('{$seqName}'));";
        }
        $globalAssignmentsQuery = self::getGlobalAssignments($entityTable);
        $globalEntityIdAssignmentQuery = '';
        if ($entityTable->hasLocalId) {
            $globalEntityIdAssignmentQuery .= self::getGlobalAssignment($entityTable);
        }
        $body = <<<SQL
{$globalAssignmentsQuery}
IF NEW.type = 1 THEN
  -- Create global entity ID when change has 'create' type
  {$createGlobalIdQuery}
ELSE
  -- Get global entity IDs if not set
  {$globalEntityIdAssignmentQuery}
END IF;
SQL;

        return self::wrap(
            $entityChangesTable->name,
            'before',
            'insert',
            $body,
            $declaration
        );
    }

    private static function getGlobalAssignments($entityTable)
    {
        $refCols = self::getRefColumns($entityTable);
        $query = '';
        foreach ($refCols as $col) {
            $query .= self::getGlobalAssignment($col->options['ref_for']);
        }
        return $query;
    }

    private static function getRefColumns($entityTable)
    {
        $res = [];
        foreach ($entityTable->entityParams as $param) {
            if (isset($param->options['ref_for']) && $param->options['ref_for']->hasLocalId) {
                array_push($res, $param);
            }
        }
        return $res;
    }

    private static function getGlobalAssignment($entityTable)
    {
        $entityColName = $entityTable->getExternalIdName();
        $entityChangesName = $entityTable->getChangesTableName();
        $storageId = Storages::getInstance()->getExternalIdName();
        $localColName = $entityTable->getLocalIdName();
        return <<<SQL

    IF NEW.{$entityColName} IS NULL THEN
    NEW.{$entityColName} = (
            SELECT {$entityColName} FROM {$entityChangesName} 
            WHERE {$storageId}=NEW.{$storageId} AND {$localColName}=NEW.{$localColName} AND type=1
          );
    END IF;
SQL;
    }

    public static function wrap($tableName, $momentName, $operationName, $body, $declaration = '')
    {

        $tableNameUCC = self::_snakeToUpperCamel($tableName);
        $momentNameUCC = self::_snakeToUpperCamel(strtolower($momentName));
        $operationNameUCC = self::_snakeToUpperCamel(strtolower($operationName));
        $momentNameUC = strtoupper($momentName);
        $operationNameUC = strtoupper($operationName);

        $res = <<<SQL
CREATE FUNCTION TRFN_{$momentNameUCC}{$tableNameUCC}{$operationNameUCC}() RETURNS trigger AS $$

SQL;
        if (strlen(trim($declaration)) > 0) {
            $dec = '  ' . str_replace("\n", "\n  ", $declaration);
            $res .= <<<SQL
DECLARE 
{$dec}

SQL;
        }
        $bod = '  ' . str_replace("\n", "\n  ", $body);
        $res .= <<<SQL
BEGIN
{$bod}
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_{$momentNameUCC}{$tableNameUCC}{$operationNameUCC}
{$momentNameUC} {$operationNameUC} ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE TRFN_{$momentNameUCC}{$tableNameUCC}{$operationNameUCC}();
SQL;
        return $res;
    }

    static function _snakeToUpperCamel($val)
    {
        $val = str_replace(' ', '', ucwords(str_replace('_', ' ', $val)));
        //$val = strtolower(substr($val,0,1)).substr($val,1);
        return $val;
    }

    public static function afterEntityChangesTrigger($entityTable, $entityChangesTable)
    {
        //TODO: При добавлении сущности, в связанных таблицах изменений глобальные IDы должны обновляться в соответствии с локальными

        $declaration = <<<SQL
parameter_last_update TIMESTAMP;
rw INTEGER;
SQL;
        $tableNameBigintParameters = ParametersBigint::getInstance()->name;
        $entityId = $entityTable->getExternalIdName();
        $updateAffectedQuery = $entityTable->updateAffectedUsersQueries('NEW.' . $entityId, 'NEW.id');

        $manipulateEntityTableQuery = self::getEntityTableManipulations($entityTable);
        $triggerAfter = $entityChangesTable->triggerAfter;
        $body = <<<SQL
{$manipulateEntityTableQuery}
{$triggerAfter}
-- Marking affected users
{$updateAffectedQuery}
-- Setting last change parameter value
UPDATE {$tableNameBigintParameters} SET value=NEW.id WHERE name='last_change';
SQL;


        return self::wrap(
            $entityChangesTable->name,
            'after',
            'insert',
            $body,
            $declaration
        );
    }

    private static function getEntityTableManipulations($entityTable)
    {
        if (!$entityTable->tableExists) {
            return '';
        }

        $tableName = $entityTable->name;
        $idColumnName = Column::getIdColumn()->name;
        $externalIdColumnName = $entityTable->getExternalIdName();
        $updateEntityTableQuery = '';
        $changesTableName = $entityTable->getChangesTableName();
        foreach ($entityTable->entityParams as $param) {
            $paramName = $param->name;
            $updateEntityTableQuery .= <<<SQL
            
IF NEW.{$paramName} IS NOT NULL THEN
    parameter_last_update := (SELECT time FROM {$changesTableName} WHERE {$externalIdColumnName}=NEW.{$externalIdColumnName} AND id!=NEW.id AND {$paramName} IS NOT NULL ORDER BY time DESC LIMIT 1);
    IF parameter_last_update IS NOT NULL AND parameter_last_update<NEW.time THEN
         UPDATE {$tableName} SET {$paramName}=NEW.{$paramName} WHERE {$idColumnName}=NEW.{$externalIdColumnName};
    END IF;
END IF;
SQL;
        }
        return <<<SQL
-- Manipulating entity table
IF NEW.type=3 THEN
  UPDATE {$tableName} SET deleted=TRUE WHERE {$idColumnName}=NEW.{$externalIdColumnName};
ELSIF NEW.type=2 THEN
  {$updateEntityTableQuery}
END IF;
SQL;
    }
}