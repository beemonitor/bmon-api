<?php

namespace helpers;

class Table
{
    public $name = '';
    public $singleName = '';
    public $hasLocalId = false;
    public $tableExists = false;
    public $entityParams = [];
    public $triggerAfter = '';
    protected $columns = [];

    /*
        public function rowVarName() {
            return 'rw_'.$this->getSingleName();
        }
    */

    final public static function getInstance()
    {
        static $instances = array();

        $calledClass = get_called_class();

        if (!isset($instances[$calledClass])) {
            $instances[$calledClass] = new $calledClass();
        }

        return $instances[$calledClass];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSingleName()
    {
        return $this->singleName;
    }

    public function getColumns()
    {
        $res = '';
        $notFirst = false;
        foreach ($this->columns as $column) {
            if ($notFirst) {
                $res .= ',';
            } else {
                $notFirst = true;
            }
            $res .= $column->name . ' ' .
                $column->type;
            foreach ($column->options['constraints'] as $constraint) {
                $res .= ' ' . $constraint;
            }
            if ($column->options['not_null']) {
                $res .= ' NOT NULL';
            }
        }
        return $res;
    }

    public function getExternalIdName()
    {
        return $this->singleName . '_id';
    }

    public function wrapCreateChangesTableColumns($cols)
    {
        return $this->wrapCreateTableColumns($cols, $this->getChangesTableName());
    }

    public function wrapCreateTableColumns($cols, $name = null)
    {
        $tblName = $this->name;
        if (isset($name)) {
            $tblName = $name;
        }
        return
            <<<SQL
            
CREATE TABLE {$tblName} (
{$cols}
);

SQL;
    }

    public function getChangesTableName()
    {
        return $this->name . '_changes';
    }

    public function getLocalIdColumn($globalColumn)
    {
        if (!$this->hasLocalId) {
            return null;
        }
        return new Column(
            $this->getLocalIdName(),
            'INTEGER',
            [
                'not_null' => isset($globalColumn->options['not_null']) ? $globalColumn->options['not_null'] : false
            ]
        );
    }

    public function getLocalIdName()
    {
        return $this->singleName . '_' . Column::LOCAL . '_id';
    }

    /*
        public function entityRowTypeDeclaration() {
            return <<<SQL
    DECLARE ;
    SQL;
    
        }
    */

    public function updateAffectedUsersQueries($entityId, $changeId)
    {
        throw new \Exception('Not implemented yet');
    }
}