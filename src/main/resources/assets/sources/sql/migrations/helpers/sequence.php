<?php

namespace helpers;


class Sequence
{
    /**
     * Возвращает имя последовательности для таблицы сущности, или заданного имени
     * @param $entity Table сущность
     * @param null $name String имя, для которого требуется последовательность. Если указано, параметр "сущность" игнорируется.
     * @return \SplString имя последовательности значений
     */
    public static function getSequenceName($entity, $name = null)
    {
        $sName = '';
        if (isset($name)) {
            $sName = $name;
        } else {
            $sName = $entity->name;
        }
        return $sName . '_ids';
    }
}