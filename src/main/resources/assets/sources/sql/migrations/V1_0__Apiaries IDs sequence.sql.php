<?php

$scope->comment(
    variables\Versions::V0_0_1,
    'Последовательность глобальных идентификаторов пасек.'
//'Таблица пасек.'
);

//$scope->table(tables\Apiaries::getInstance());

$seqName = \helpers\Sequence::getSequenceName(\tables\Apiaries::getInstance());

$scope->write(
    <<<SQL
    CREATE SEQUENCE {$seqName} START 1;
SQL
);