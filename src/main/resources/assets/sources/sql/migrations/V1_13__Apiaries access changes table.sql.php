<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица изменений сущностей типа: Доступ к объекту типа Пасека.'
);

$scope->table(\tables\ApiariesAccessChanges::getInstance());