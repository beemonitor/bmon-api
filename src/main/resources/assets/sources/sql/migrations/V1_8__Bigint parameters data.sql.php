<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Добавление первых параметров в таблицу системных параметров типа BIGINT.'
);

$scope->write(
    <<<SQL
INSERT INTO 
SQL
    . \tables\ParametersBigint::getInstance()->getName()
    . <<<SQL
 (name, value) VALUES
    -- Значение последнего идентификатора изменений, при котором были сфрмированы новые файлы изменений для клиентских устройств
    ('last_packed_change', 0),
    -- Идентификатор последнего внесённого в систему изменения
    ('last_change',0);
SQL
);