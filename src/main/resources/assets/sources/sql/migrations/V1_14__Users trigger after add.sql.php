<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Триггер после добавления новых записей в таблицу пользователей.'
);

$affectedUsersName = \tables\AffectedUsers::getInstance()->name;
$changesFilesName = \tables\ChangesFiles::getInstance()->name;
$usersColumnName = \tables\Users::getInstance()->getExternalIdName();
$changeColumnName = \parts\Changes::COLUMN_NAME;

$scope->writeAfterWF(
    \helpers\Trigger::wrap(
        \tables\Users::getInstance()->name,
        'after',
        'insert',
        <<<SQL
  INSERT INTO {$affectedUsersName} ({$usersColumnName}, {$changeColumnName}) VALUES (NEW.id, 0);
INSERT INTO {$changesFilesName} ({$usersColumnName}, start_id, end_id) VALUES (NEW.id, 0, 0);
SQL
    )
);