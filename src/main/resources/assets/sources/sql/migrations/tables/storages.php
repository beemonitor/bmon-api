<?php

namespace tables;

use helpers\Column as Col;

class Storages extends \helpers\Table
{
    public $name = 'storages';
    public $singleName = 'storage';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            Col::getRefColumn(Devices::getInstance(), true),
            Col::getRefColumn(Users::getInstance(), true)
        );
    }
}