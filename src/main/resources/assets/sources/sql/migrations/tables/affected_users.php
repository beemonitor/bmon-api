<?php

namespace tables;

use helpers\Column as Col;
use helpers\Table;
use parts\Changes;


class AffectedUsers extends Table
{
    public $name = 'users_affected';
    public $singleName = 'user_affected';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getRefColumn(Users::getInstance(), true, null, [
                'PRIMARY KEY'
            ]),
            Changes::getIdColumn(true)
        );
    }
}