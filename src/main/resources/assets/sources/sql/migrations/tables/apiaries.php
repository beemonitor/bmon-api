<?php

namespace tables;

use helpers\Column as Col;
use helpers\Table;
use parts\Changes;

class Apiaries extends Table
{
    public $name = 'apiaries';
    public $singleName = 'apiary';
    public $hasLocalId = true;
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn()
        );
        $this->entityParams = [
            new Col(
                'name',
                'VARCHAR(128)',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                'description',
                'VARCHAR(512)'
            )
        ];
        $this->columns = $this->columns = array_merge($this->columns, $this->entityParams);
        array_push(
            $this->columns,
            Col::getDeletedColumn()
        );
    }

    public function updateAffectedUsersQueries($apiaryId, $changeId)
    {
        $apiariesAccessTableName = ApiariesAccess::getInstance()->name;
        $apiaryIdName = Apiaries::getInstance()->getExternalIdName();
        $userIdName = Users::getInstance()->getExternalIdName();
        $affectedUsersName = AffectedUsers::getInstance()->name;
        $changeIdName = Changes::COLUMN_NAME;
        return <<<SQL
FOR rw IN SELECT {$userIdName} FROM {$apiariesAccessTableName} WHERE {$apiaryIdName}={$apiaryId}
    LOOP
    UPDATE {$affectedUsersName} SET {$changeIdName}={$changeId} WHERE {$userIdName}=rw;
END LOOP;
SQL;
    }
}