<?php

namespace tables;

use helpers\ColumnSets;
use helpers\Table;
use parts\Changes;

class ApiariesAccessChanges extends Table
{
    public $name = 'apiaries_access_changes';
    public $singleName = 'apiary_access_change';
    protected $columns = [];

    protected function __construct()
    {
        $this->columns = array_merge($this->columns, ColumnSets::getForEntityChanges(ApiariesAccess::getInstance()));
        $this->triggerAfter = $this->getTriggerAfter();
    }


    private function getTriggerAfter()
    {
        $linkedUsersName = LinkedUsers::getInstance()->name;
        $tableName = $this->name;
        $userIdCol = Users::getInstance()->getExternalIdName();
        $apiaryIdCol = Apiaries::getInstance()->getExternalIdName();
        $changeIdCol = Changes::getIdColumn()->name;
        return <<<SQL
-- Adding linked users
IF NEW.type=1 THEN
    IF NEW.read_only=FALSE THEN
     FOR rw IN SELECT {$userIdCol} FROM {$tableName} WHERE {$apiaryIdCol}=NEW.{$apiaryIdCol} AND {$userIdCol}!=NEW.{$userIdCol}
          LOOP
          INSERT INTO {$linkedUsersName} ({$userIdCol},linked_{$userIdCol},{$changeIdCol}) VALUES (rw,NEW.{$userIdCol},NEW.id) ON CONFLICT DO NOTHING;
      END LOOP;
    END IF;
    FOR rw IN SELECT {$userIdCol} FROM {$tableName} WHERE {$apiaryIdCol}=NEW.{$apiaryIdCol} AND {$userIdCol}!=NEW.{$userIdCol}
          LOOP
          INSERT INTO {$linkedUsersName} ({$userIdCol},linked_{$userIdCol},{$changeIdCol}) VALUES (NEW.{$userIdCol},rw,NEW.id) ON CONFLICT DO NOTHING;
    END LOOP;
END IF;
SQL;
    }
}