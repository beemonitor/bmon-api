<?php
/**
 * Created by IntelliJ IDEA.
 * User: R5
 * Date: 27.07.2016
 * Time: 14:24
 */

namespace tables;

use helpers\ColumnSets;
use helpers\Table;

class ApiariesChanges extends Table
{
    public $name = 'apiaries_changes';
    public $singleName = 'apiary_change';
    protected $columns = [];

    protected function __construct()
    {
        $this->columns = array_merge(
            $this->columns,
            ColumnSets::getForEntityChanges(Apiaries::getInstance())
        );
    }
}