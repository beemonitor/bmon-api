<?php

namespace tables;

use helpers\Column as Col;

class Devices extends \helpers\Table
{
    public $name = 'devices';
    public $singleName = 'device';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            new Col(
                'name',
                'VARCHAR(128)',
                [
                    'not_null' => true
                ]
            )
        );
    }
}