<?php

namespace tables;

use helpers\Column as Col;
use helpers\Table;
use parts\Changes;

class ChangesFiles extends Table
{
    public $name = 'changes_files';
    public $singleName = 'changes_file';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            Col::getRefColumn(Users::getInstance(), true),
            Col::getRefColumn(Storages::getInstance()),
            Changes::getIdColumn(true, 'start_id'),
            Changes::getIdColumn(true, 'end_id'),
            new Col(
                'path',
                'VARCHAR(512)'
            )
        );
    }
}