<?php

namespace tables;

use helpers\Column as Col;

class Users extends \helpers\Table
{
    public $name = 'users';
    public $singleName = 'user';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            Col::getIdColumn(),
            new Col(
                'name',
                'VARCHAR(64)',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                'email',
                'VARCHAR(255)',
                [
                    'constraints' => ['UNIQUE'],
                    'not_null' => true
                ]
            ),
            new Col(
                'password',
                'CHAR(60)',
                [
                    'not_null' => true
                ]
            ),
            new Col(
                'session',
                'BYTEA',
                [
                    'not_null' => true
                ]
            )
        );
    }
}