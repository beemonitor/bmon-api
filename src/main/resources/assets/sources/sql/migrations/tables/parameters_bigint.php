<?php
/**
 * Created by IntelliJ IDEA.
 * User: R5
 * Date: 27.07.2016
 * Time: 13:31
 */

namespace tables;

use helpers\Column as Col;

class ParametersBigint extends \helpers\Table
{
    public $name = 'parameters_bigint';
    public $singleName = 'parameter_bigint';
    protected $columns = [];

    protected function __construct()
    {
        array_push(
            $this->columns,
            new Col(
                'name',
                'VARCHAR(128)',
                [
                    'constraints' => ['PRIMARY KEY']
                ]
            ),
            new Col(
                'value',
                'BIGINT'
            )
        );
    }
}