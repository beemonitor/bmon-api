<?php
namespace parts;

class Basic
{

    public static function getHeaderComment($version, $comment)
    {
        return
            <<<SQL
            /**
{$version}
{$comment}

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
SQL;
    }
}