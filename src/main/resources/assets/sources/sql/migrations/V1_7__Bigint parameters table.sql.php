<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Таблица системных параметров вида "ключ:строка -> значение:bigint".'
);

$scope->table(\tables\ParametersBigint::getInstance());