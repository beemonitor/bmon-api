<?php

$scope->comment(
    variables\Versions::V0_0_4,
    'Последовательность идентификаторов изменений.
Данная последовательность идентификаторов связана со всеми изменениями сущностей в системе.'
);

$seqName = \helpers\Sequence::getSequenceName(null, \parts\Changes::NAME);

$scope->write(
    <<<SQL
CREATE SEQUENCE {$seqName} START 1;
SQL
);