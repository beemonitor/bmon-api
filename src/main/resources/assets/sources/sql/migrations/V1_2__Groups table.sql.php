<?php

$scope->comment(
    variables\Versions::V0_0_2,
    'Таблица групп пользователей.'
);

$scope->table(\tables\UsersGroups::getInstance());