/**
v0.0.4
Стартовая структура для БД с набором изменений, отправляемым мобильному приложению.
Этот файл не используется сам по себе. Он лишь является основой для файла формата .db
- основы БД списка изменений, отправляемого мобильному приложению.
 */

CREATE TABLE apiaries_changes (
  global_id       BIGINT PRIMARY KEY,
  time            TIMESTAMP NOT NULL,
  type            SMALLINT  NOT NULL,
  user_id         BIGINT    NOT NULL,

  apiary_id       INTEGER,
  apiary_id_local INTEGER,
  name            VARCHAR(128),
  description     VARCHAR(512),

  CHECK (type != 1 OR name IS NOT NULL),
  CHECK ((type != 2 AND type != 3) OR apiary_id IS NOT NULL OR apiary_id_local IS NOT NULL),
  CHECK (apiary_id IS NOT NULL OR apiary_id_local IS NOT NULL)
);

CREATE TABLE users_linked (
  id        INTEGER      NOT NULL PRIMARY KEY,
  name      VARCHAR(64)  NOT NULL,
  email     VARCHAR(255) NOT NULL UNIQUE,
  change_id BIGINT       NOT NULL
);