/**
v0.0.4
Таблица с информацией о последних изменениях, затронувших конкретных пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE users_affected (
  user_id   INTEGER REFERENCES users (id) PRIMARY KEY NOT NULL,
  change_id BIGINT                                    NOT NULL
);
