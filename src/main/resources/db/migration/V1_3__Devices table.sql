/**
v0.0.4
Таблица клиентских устройств.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE devices (
  id   SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL
);
