/**
v0.0.4
Триггер после добавления новых записей в таблицу пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE FUNCTION TRFN_BeforeApiariesChangesInsert()
  RETURNS TRIGGER AS $$
BEGIN

  IF NEW.type = 1
  THEN
    -- Create global entity ID when change has 'create' type
    NEW.apiary_id = (SELECT nextval('apiaries_ids'));
  ELSE
    -- Get global entity IDs if not set

    IF NEW.apiary_id IS NULL
    THEN
      NEW.apiary_id = (
        SELECT apiary_id
        FROM apiaries_changes
        WHERE storage_id = NEW.storage_id AND apiary_local_id = NEW.apiary_local_id AND type = 1
      );
    END IF;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_BeforeApiariesChangesInsert
BEFORE INSERT ON apiaries_changes
FOR EACH ROW
EXECUTE PROCEDURE TRFN_BeforeApiariesChangesInsert();


CREATE FUNCTION TRFN_AfterApiariesChangesInsert()
  RETURNS TRIGGER AS $$
DECLARE
  parameter_last_update TIMESTAMP;
  rw                    INTEGER;
BEGIN


  -- Marking affected users
  FOR rw IN SELECT user_id
            FROM apiaries_access
            WHERE apiary_id = NEW.apiary_id
  LOOP
    UPDATE users_affected
    SET change_id = NEW.id
    WHERE user_id = rw;
  END LOOP;
  -- Setting last change parameter value
  UPDATE parameters_bigint
  SET value = NEW.id
  WHERE name = 'last_change';
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_AfterApiariesChangesInsert
AFTER INSERT ON apiaries_changes
FOR EACH ROW
EXECUTE PROCEDURE TRFN_AfterApiariesChangesInsert();