/**
v0.0.4
Таблица системных параметров вида "ключ:строка -> значение:bigint".

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE parameters_bigint (
  name  VARCHAR(128) PRIMARY KEY,
  value BIGINT
);
