/**
v0.0.2
Таблица групп пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE users_groups (
  id       SERIAL PRIMARY KEY,
  user_id  INTEGER REFERENCES users (id) NOT NULL,
  group_id SMALLINT                      NOT NULL,
  UNIQUE (user_id, group_id)
);
