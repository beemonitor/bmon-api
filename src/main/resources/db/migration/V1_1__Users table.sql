/**
v0.0.2
Таблица пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE users (
  id       SERIAL PRIMARY KEY,
  name     VARCHAR(64)         NOT NULL,
  email    VARCHAR(255) UNIQUE NOT NULL,
  password CHAR(60)            NOT NULL,
  session  BYTEA               NOT NULL
);
