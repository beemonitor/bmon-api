/**
v0.0.4
Таблица с информацией о сформированных для клиентских устройств файлах изменений.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE changes_files (
  id         SERIAL PRIMARY KEY,
  user_id    INTEGER REFERENCES users (id) NOT NULL,
  storage_id INTEGER REFERENCES storages (id),
  start_id   BIGINT                        NOT NULL,
  end_id     BIGINT                        NOT NULL,
  path       VARCHAR(512)
);
