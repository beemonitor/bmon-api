/**
v0.0.4
Таблица локальных хранилищ.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE storages (
  id        SERIAL PRIMARY KEY,
  device_id INTEGER REFERENCES devices (id) NOT NULL,
  user_id   INTEGER REFERENCES users (id)   NOT NULL
);
