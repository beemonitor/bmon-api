/**
v0.0.4
Таблица связанных друг с другом пользователей.

Этот файл сгенерирован автоматически!
Файлы редактируемых шаблонов размещены в папке "assets/sources/sql".
*/
CREATE TABLE users_linked (
  id             SERIAL PRIMARY KEY,
  user_id        INTEGER REFERENCES users (id) NOT NULL,
  linked_user_id INTEGER REFERENCES users (id) NOT NULL,
  change_id      BIGINT                        NOT NULL,
  UNIQUE (user_id, linked_user_id)
);
