/*
Пароли - хэши bcrypt с Work Factor = 6
"crAjaq2h" - $2a$06$kfcHjvT871.KTQMobJdqGOYPHAu5T0Lmwh9Xzly.qxMYMQmYhpat2
"hatred" = $2a$06$SIZK4U1HVVw0bUUDz4VveuJmSWkwev8YI1jibT9tS5/Kly50p4ngG
"привет медвед" = $2a$06$OMOmx1F7c.kaY9RH5Eul1.Br1W7jWVTU8kR.Hxegi2j3prXp3zGme
"h@Ck3r" = $2a$06$t.P4s99aT62PSluVcqU9vO7bZTWr0RkUN232IlAzYWeME5KBFHRtm
 */
INSERT INTO users (name, email, password, session) VALUES
  ('raptor985', 'raptor985@ya.ru', '$2a$06$kfcHjvT871.KTQMobJdqGOYPHAu5T0Lmwh9Xzly.qxMYMQmYhpat2', 'MJsRUZ0w2A33QLQ0'),
  ('Tester', 'hat@bmon.dev', '$2a$06$SIZK4U1HVVw0bUUDz4VveuJmSWkwev8YI1jibT9tS5/Kly50p4ngG', 'AXV0FxMCZY3IJWYJ'),
  ('Русский тестер', 'ivan54rus@bmon.dev', '$2a$06$OMOmx1F7c.kaY9RH5Eul1.Br1W7jWVTU8kR.Hxegi2j3prXp3zGme',
   'hhF5M7ImC5Hb9l9L'),
  ('Максимка a.k.a. SuP3Rh@cKeR', 'r3cky0urc0de@bmon.dev',
   '$2a$06$t.P4s99aT62PSluVcqU9vO7bZTWr0RkUN232IlAzYWeME5KBFHRtm', 'rt690u5TNSE7P5dK');

INSERT INTO users_groups (user_id, group_id) VALUES
  ((SELECT id
    FROM users
    WHERE email = 'raptor985@ya.ru'), 0),
  ((SELECT id
    FROM users
    WHERE email = 'hat@bmon.dev'), 1),
  ((SELECT id
    FROM users
    WHERE email = 'ivan54rus@bmon.dev'), 1),
  ((SELECT id
    FROM users
    WHERE email = 'r3cky0urc0de@bmon.dev'), 1);