/**
v0.0.4
Заполнение таблицы локальных хранилищ тестовыми данными
 */

INSERT INTO storages (device_id, user_id) VALUES
  (
    (SELECT id
     FROM devices
     WHERE name = 'Android 4.2 - ALCATEL One Touch POP C9 7047D'),
    (SELECT id
     FROM users
     WHERE email = 'hat@bmon.dev')
  ),
  (
    (SELECT id
     FROM devices
     WHERE name = 'Android 4.2 - ALCATEL One Touch POP C9 7047D'),
    (SELECT id
     FROM users
     WHERE email = 'ivan54rus@bmon.dev')
  ),
  (
    (SELECT id
     FROM devices
     WHERE name = 'iOS 9.7 - iToaster 4S Limited edition'),
    (SELECT id
     FROM users
     WHERE email = 'hat@bmon.dev')
  ),
  (
    (SELECT id
     FROM devices
     WHERE name = 'iOS 9.7 - iToaster 4S Limited edition'),
    (SELECT id
     FROM users
     WHERE email = 'raptor985@ya.ru')
  ),
  (
    (SELECT id
     FROM devices
     WHERE name = 'iOS 9.7 - iToaster 4S Limited edition'),
    (SELECT id
     FROM users
     WHERE email = 'r3cky0urc0de@bmon.dev')
  );