/**
v0.0.4
Добавление доступа для других юзеров к стартовым пасекам
 */

INSERT INTO apiaries_access_changes (storage_id, local_id, time, type, apiary_access_local_id, user_id, apiary_local_id, read_only)
VALUES
  ((SELECT s.id
    FROM users u LEFT JOIN storages s ON s.user_id = u.id
    WHERE u.email = 'raptor985@ya.ru'
    LIMIT 1), (SELECT nextval('test_local_changes_2_seq')), NOW(), 1, 1,
   (SELECT id
    FROM users
    WHERE email = 'hat@bmon.dev'), (SELECT apiary_local_id
                                    FROM apiaries_changes
                                    WHERE name = 'Марусино'
                                    LIMIT 1), TRUE),

  ((SELECT s.id
    FROM users u LEFT JOIN storages s ON s.user_id = u.id
    WHERE u.email = 'raptor985@ya.ru'
    LIMIT 1), (SELECT nextval('test_local_changes_2_seq')), NOW(), 1, 1,
   (SELECT id
    FROM users
    WHERE email = 'hat@bmon.dev'), (SELECT apiary_local_id
                                    FROM apiaries_changes
                                    WHERE name = 'Пасека под Майским'
                                    LIMIT 1), FALSE),

  ((SELECT s.id
    FROM users u LEFT JOIN storages s ON s.user_id = u.id
    WHERE u.email = 'raptor985@ya.ru'
    LIMIT 1), (SELECT nextval('test_local_changes_2_seq')), NOW(), 1, 2,
   (SELECT id
    FROM users
    WHERE email = 'raptor985@ya.ru'), (SELECT apiary_local_id
                                       FROM apiaries_changes
                                       WHERE name = 'Пасека в Кудряшовском' AND type = 1), FALSE),

  ((SELECT s.id
    FROM users u LEFT JOIN storages s ON s.user_id = u.id
    WHERE u.email = 'raptor985@ya.ru'
    LIMIT 1), (SELECT nextval('test_local_changes_2_seq')), NOW(), 1, 3,
   (SELECT id
    FROM users
    WHERE email = 'raptor985@ya.ru'), (SELECT apiary_local_id
                                       FROM apiaries_changes
                                       WHERE name = 'Луговая пасека' AND type = 1), FALSE);