/**
v0.0.4
Тестовые данные пасек
 */

INSERT INTO apiaries_changes (local_id, type, time, storage_id, apiary_local_id, name, description) VALUES
  ((SELECT nextval('test_local_changes_1_seq')), 1, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'hat@bmon.dev'
                                                            LIMIT 1), 1, 'Пасека в Кудряшовском',
   'Кудряшовский — посёлок в Новосибирском районе Новосибирской области России, центр Кудряшовского сельсовета.'),

  ((SELECT nextval('test_local_changes_2_seq')), 1, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'raptor985@ya.ru'
                                                            LIMIT 1), 1, 'НЕ Марусино',
   'Мару́сино — деревня в Люберецком районе Московской области. Население — 490 чел.'),

  ((SELECT nextval('test_local_changes_2_seq')), 2, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'raptor985@ya.ru'
                                                            LIMIT 1), 1, 'Марусино',
   NULL),

  ((SELECT nextval('test_local_changes_2_seq')), 1, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'raptor985@ya.ru'
                                                            LIMIT 1), 2, 'Пасека под Майским',
   'Ма́йские хрущи́ — род насекомых семейства пластинчатоусых, обитающих в Европе и Азии. Вплоть до середины 1950-х годов были очень широко распространены и, являясь вредителями растений, в отдельные годы приносили существенный ущерб сельскому хозяйству.'),

  ((SELECT nextval('test_local_changes_1_seq')), 1, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'hat@bmon.dev'
                                                            LIMIT 1), 2, 'Луговая пасека',
   'Луг — в широком смысле — тип зональной и интразональной растительности, характеризующийся господством многолетних травянистых растений, главным образом злаков и осоковых, в условиях достаточного или избыточного увлажнения.'),

  ((SELECT nextval('test_local_changes_2_seq')), 1, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'raptor985@ya.ru'
                                                            LIMIT 1), 3, 'Тестовая', ''),
  ((SELECT nextval('test_local_changes_2_seq')), 3, NOW(), (SELECT s.id
                                                            FROM users u LEFT JOIN storages s ON s.user_id = u.id
                                                            WHERE u.email = 'raptor985@ya.ru'
                                                            LIMIT 1), 3, NULL, NULL);