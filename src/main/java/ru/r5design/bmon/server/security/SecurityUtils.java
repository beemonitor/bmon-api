package ru.r5design.bmon.server.security;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.r5design.bmon.server.model.User;

/**
 * Вспомогательный класс для работы с авторизацией
 */
public class SecurityUtils {

    public static User getCurrentUser() {
        return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
