package ru.r5design.bmon.server.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import ru.r5design.bmon.server.config.AclConfig;

import static ru.r5design.bmon.server.security.AclUtils.AclRoles.*;

/**
 * Created by R5 on 20.06.2016.
 */
public class AclUtils {
    /**
     * Выстраивает список ограничений Spring Security в соответствии с ACL
     * @param registry
     * @return
     */
    public static
    ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry
    buildAclPaths(
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry
    ) {
        Object[][] constraints = AclConfig.getAclConstraints();
        for (Object[] pathRule : constraints) {
            //Игнорируем случайно оставленную в конце запятую
            if (pathRule == null) {
                continue;
            }
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl matcher;
            matcher = registry.antMatchers((HttpMethod)pathRule[1],(String)pathRule[0]);
            if (((AclRoles)pathRule[2]).compareTo(AUTHORIZED)==0) {
                registry = matcher.authenticated();
            }
            if (((AclRoles)pathRule[2]).compareTo(ANY_USER)==0) {
                registry = matcher.permitAll();
            }
            else {
                String[] roles = new String[pathRule.length - 2];
                for (int i = 2; i < pathRule.length; i++) {
                    roles[i - 2] = ((AclRoles) pathRule[i]).getRoleName();
                }
                registry = matcher.hasAnyRole(roles);
            }
        }
        //Все не указанные в ACL пути доступны только для админа.
        //Не внёс новый функционал в ACL - сам себе злобный буратино.
        registry.anyRequest().hasRole(ADMIN.getRoleName());
        return registry;
    }

    public enum AclRoles {
        USER,
        ADMIN,
        ANONYMOUS,
        AUTHORIZED,
        ANY_USER;

        public String getRoleName() {
            if (this.compareTo(USER) == 0) {
                return "USER";
            }
            if (this.compareTo(ADMIN) == 0) {
                return "ADMIN";
            }
            if (this.compareTo(ANONYMOUS) == 0) {
                return "ANONYMOUS";
            }
            return null;
        }
    }
}
