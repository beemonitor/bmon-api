package ru.r5design.bmon.server.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by R5 on 19.06.2016.
 */
public class NamedAuthority implements GrantedAuthority {
    String name;

    public NamedAuthority(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
