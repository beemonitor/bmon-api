package ru.r5design.bmon.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Контроллер страниц, косвенно связынных с функциями API
 */
@Controller
public class MiscController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "pages/root";
    }

}
