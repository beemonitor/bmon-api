package ru.r5design.bmon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.dataform.*;
import ru.r5design.bmon.server.model.ChangesFile;
import ru.r5design.bmon.server.model.Device;
import ru.r5design.bmon.server.model.Storage;
import ru.r5design.bmon.server.repository.ChangesFileRepository;
import ru.r5design.bmon.server.repository.DeviceRepository;
import ru.r5design.bmon.server.repository.StorageRepository;
import ru.r5design.bmon.server.security.SecurityUtils;

/**
 * Контроллер функций для клиентских устройств
 */
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    DeviceRepository deviceRepository;
    @Autowired
    StorageRepository storageRepository;
    @Autowired
    ChangesFileRepository changesFileRepository;

    @Autowired
    private GlobalConfig globalConfig;


    @RequestMapping(value = "/device/add", method = RequestMethod.POST)
    public JSONResponseWrapper<DeviceDataJSON> deviceAdd(
            @RequestParam("name") String name
    ) {
        Device d = new Device();
        d.setName(name);
        deviceRepository.save(d);
        JSONResponseWrapper<DeviceDataJSON> res = new JSONResponseWrapper<>(new DeviceDataJSON(), globalConfig);
        res.getResponse().deviceId = d.getId();
        return res;
    }

    @RequestMapping(value = "/storage/add", method = RequestMethod.POST)
    public JSONResponseWrapper<StorageDataJSON> storageAdd(
            @RequestParam("device_id") Long deviceId
    ) {
        Long userId = SecurityUtils.getCurrentUser().getId();
        Storage s = new Storage();
        s.setUserId(userId);
        s.setDeviceId(deviceId);
        storageRepository.save(s);
        StorageDataJSON storageData = new StorageDataJSON();
        storageData.storageId = s.getId();
        JSONResponseWrapper<StorageDataJSON> res = new JSONResponseWrapper<>(storageData, globalConfig);
        return res;
    }

    @RequestMapping(value = "/data/initial", method = RequestMethod.GET)
    public JSONResponseWrapper<ChangesetJSON> getInitialData() {
        ChangesFile cf = changesFileRepository.findInitialForUser(SecurityUtils.getCurrentUser().getId());
        ChangesetJSON cs = new ChangesetJSON();
        String filePath = cf.getPath();
        if (filePath == null) {
            cs.setType(ChangesetType.EMPTY);
        } else {
            cs.setType(ChangesetType.LINK);
            cs.link = globalConfig.getChangesUrl() + '/' + filePath;
        }
        return new JSONResponseWrapper<ChangesetJSON>(cs, globalConfig);
    }

}
