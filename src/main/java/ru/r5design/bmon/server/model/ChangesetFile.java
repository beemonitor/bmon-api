package ru.r5design.bmon.server.model;

import org.apache.commons.codec.binary.Base32;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс для работы с файлом списка изменений
 */
public class ChangesetFile {

    final int QUERY_TIMEOUT = 30;
    final String SECRET = "9DGYftvdNxpP0y69";
    final String baseSqlFilePath;
    Connection connection;
    String filePath = null;
    String fileName = null;

    /**
     * @param folder          папка, в которой разместить файл набора изменений
     * @param fileNameBase    базовое (незашифрованное) имя файла. Важно, чтобы имена файлов для разных наборов <b>не повторялись</b>
     * @param baseSqlFilePath Путь к инициализационному файлу формата БД SQLite, в копию которого будут помещаться наборы изменений
     * @throws SQLException
     */
    public ChangesetFile(String folder, String fileNameBase, String fileNamePostfix, String baseSqlFilePath) throws Exception {
        filePath = folder;
        fileName = encryptFileName(fileNameBase) + fileNamePostfix + ".db";
        this.baseSqlFilePath = baseSqlFilePath;
    }

    /**
     * Шифрует имя файла, для того, чтобы при выкладывании его в паблик, подобрать имя файла было непросто
     *
     * @param name незашифрованное имя файла
     * @return
     */
    String encryptFileName(final String name) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Key key = new SecretKeySpec(SECRET.getBytes(), "AES");

        // Encrypt
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedData = cipher.doFinal(name.getBytes());

        Base32 base32 = new Base32();
        String result = base32.encodeAsString(encryptedData);

        return result;
    }

    private Connection getConnection() throws SQLException, IOException {
        if (connection != null) {
            return connection;
        }
        File f = new File(filePath + fileName);
        if (!f.exists() || f.isDirectory()) {
            createInitFile();
        }
        connection = DriverManager.getConnection("jdbc:sqlite:" + filePath + fileName);
        connection.setAutoCommit(false);
        return connection;
    }

    /**
     * Создаёт стартовый файл изменений, помещая его по пути filePath+fileName
     *
     * @throws IOException
     * @throws SQLException
     */
    public void createInitFile() throws IOException, SQLException {
        final Path baseSql = Paths.get(baseSqlFilePath);
        final Path changesFile = Paths.get(filePath + fileName);
        Files.copy(baseSql, changesFile, StandardCopyOption.REPLACE_EXISTING);
    }

    public String getFileName() {
        return fileName;
    }
/*
    public void addApiaryChanges(Iterable<LinkedUser> changes) throws SQLException, IOException {
        if (changes == null || !changes.iterator().hasNext()) {
            return;
        }
        PreparedStatement ps = getConnection().prepareStatement(
                "INSERT OR REPLACE INTO apiaries_changes " +
                        "(global_id,time,type,user_id,apiary_id,name,description) VALUES (?,?,?,?,?,?,?)"
        );
        for (LinkedUser c : changes) {
            int i = 1;
            //global_id
            ps.setLong(i++, c.getId());
            //time
            ps.setTimestamp(i++, c.getTime());
            //type
            ps.setInt(i++, c.getType());
            //user_id
            ps.setLong(i++, c.getStorage().getId());
            //apiary_id
            ps.setLong(i++, c.getApiaryId());
            //name
            ps.setString(i++, c.getName());
            //description
            ps.setString(i++, c.getDescription());
            ps.addBatch();
        }
        ps.executeBatch();
    }
*/
    /**
     * Добавляет список пользователей, так или иначе, связанных с данным, в БД
     */
    /*
    public void addLinkedUsers(Iterable<LinkedUser> users) throws IOException, SQLException {
        if (users == null || !users.iterator().hasNext()) {
            return;
        }
        PreparedStatement ps = getConnection().prepareStatement(
                "INSERT OR REPLACE INTO users_linked " +
                        "(id,name,email,change_id) VALUES (?,?,?,?)"
        );
        for (LinkedUser lu : users) {
            int i = 1;
            //id
            ps.setLong(i++, lu.getLinkedUser().getId());
            //name
            ps.setString(i++, lu.getLinkedUser().getName());
            //email
            ps.setString(i++, lu.getLinkedUser().getEmail());
            //change_id
            ps.setLong(i++, lu.getChangeId());
            ps.addBatch();
        }
        ps.executeBatch();
    }
    */

    public void revertAndClose() {
        try {
            connection.rollback();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void commitAndClose() {
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
