package ru.r5design.bmon.server.model;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Справочник связей идентификаторов групп с их названиями. Не хранится в БД.
 */
public class Group {
    private static Group ourInstance = new Group();
    //Связи вида id_группы:наименование_группы
    BidiMap names;
    //Связи вида группы_предки:группа_наследник
    BidiMap relations;

    private Group() {
        names = new DualHashBidiMap();
        names.put((Short) (short) 0, List.ADMIN.getName());
        names.put((Short) (short) 1, List.USER.getName());
        relations = new DualHashBidiMap();
        //Группа "admin" наследует права группы "user"
        relations.put(new LinkedList<Short>(){{
            push(getId(List.USER.getName()));
        }}, getId(List.ADMIN.getName()));
    }

    public static Group getInstance() {
        return ourInstance;
    }

    private LinkedList<Short> getAncestors(short id) {
        return (LinkedList<Short>)relations.getKey(id);
    }

    /**
     * Получает полный список групп, включаемых в указанную (предки группы, и саму группу).
     * Группа наследует все права от своих предков.
     * @param id
     * @return
     */
    public Collection<Short> getIncludedList(short id) {
        HashSet<Short> groups = new HashSet<>();
        LinkedList<Short> groupsList = new LinkedList<Short>(){{
            push(id);
        }};
        while (groupsList.size()>0) {
            Short gId = groupsList.pollFirst();
            groups.add(gId);
            LinkedList<Short> groupAncestors = getAncestors(gId);
            if (groupAncestors!=null) {
                groupsList.addAll(groupAncestors);
            }
        }
        return groups;
    }

    public String getName(short id) {
        return (String) names.get(id);
    }

    public short getId (String name) {
        return (Short)names.getKey(name);
    }

    public enum List {
        ADMIN("admin"), USER("user");

        String name;

        List(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
