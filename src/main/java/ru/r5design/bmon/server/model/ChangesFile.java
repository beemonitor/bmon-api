package ru.r5design.bmon.server.model;

import javax.persistence.*;

/**
 * Информация о файле, хранящем список изменений для передачи клиенту
 */
@Entity
@Table(name = "changes_files")
public class ChangesFile {
    private Long id;
    private Long userId;
    private Long storageId;
    private Long startId;
    private Long endId;
    private String path;

    @Id
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "storage_id", nullable = true)
    public Long getStorageId() {
        return storageId;
    }

    public void setStorageId(Long storageId) {
        this.storageId = storageId;
    }

    @Basic
    @Column(name = "start_id", nullable = false)
    public Long getStartId() {
        return startId;
    }

    public void setStartId(Long startId) {
        this.startId = startId;
    }

    @Basic
    @Column(name = "end_id", nullable = false)
    public Long getEndId() {
        return endId;
    }

    public void setEndId(Long endId) {
        this.endId = endId;
    }

    @Basic
    @Column(name = "path", nullable = true, length = 512)
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChangesFile that = (ChangesFile) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (storageId != null ? !storageId.equals(that.storageId) : that.storageId != null) return false;
        if (startId != null ? !startId.equals(that.startId) : that.startId != null) return false;
        if (endId != null ? !endId.equals(that.endId) : that.endId != null) return false;
        if (path != null ? !path.equals(that.path) : that.path != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (storageId != null ? storageId.hashCode() : 0);
        result = 31 * result + (startId != null ? startId.hashCode() : 0);
        result = 31 * result + (endId != null ? endId.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }
}
