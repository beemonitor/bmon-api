package ru.r5design.bmon.server.model;

import java.util.HashMap;

/**
 * Перечисление типов изменений
 */
public enum ChangeType {
    CREATE(1, "create"), UPDATE(2, "update"), DELETE(3, "delete");

    static HashMap<Integer, ChangeType> cache = null;
    int type;
    String name;

    ChangeType(int type, String name) {
        this.type = type;
        this.name = name;
    }

    private static HashMap<Integer, ChangeType> getCache() {
        if (cache != null) {
            return cache;
        }
        cache = new HashMap<>();
        ChangeType[] types = {CREATE, UPDATE, DELETE};
        for (ChangeType type : types) {
            cache.put(type.getType(), type);
        }
        return cache;
    }

    public static ChangeType getForType(int type) {
        return getCache().get(type);
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
