package ru.r5design.bmon.server.model;

public class LinkedUser {
    private Long id;
    private Long user_id;
    private Long linked_user_id;
    private Long change_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getLinked_user_id() {
        return linked_user_id;
    }

    public void setLinked_user_id(Long linked_user_id) {
        this.linked_user_id = linked_user_id;
    }

    public Long getChange_id() {
        return change_id;
    }

    public void setChange_id(Long change_id) {
        this.change_id = change_id;
    }
}
