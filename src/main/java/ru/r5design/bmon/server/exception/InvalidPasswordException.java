package ru.r5design.bmon.server.exception;

/**
 * Created by R5 on 15.06.2016.
 */
public class InvalidPasswordException extends Exception {
    public InvalidPasswordException(String message) {
        super(message);
    }
}
