package ru.r5design.bmon.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.r5design.bmon.server.model.ParameterBigint;
import ru.r5design.bmon.server.repository.ParameterBigintRepository;

/**
 * Обёртка для работы с хранимыми в БД параметрами приложения
 */
@Repository
public class StoredParameters {

    @Autowired
    ParameterBigintRepository rep;

    public void set(String key, Long value) {
        ParameterBigint par = rep.findOne(key);
        if (par == null) {
            par = new ParameterBigint();
        }
        par.setValue(value);
        rep.save(par);
    }

    public Long getLong(String key) {
        ParameterBigint par = rep.findOne(key);
        if (par == null) {
            return null;
        }
        return par.getValue();
    }
}
