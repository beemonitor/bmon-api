package ru.r5design.bmon.server.config;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static ru.r5design.bmon.server.security.AclUtils.AclRoles.*;

/**
 * Created by R5 on 20.06.2016.
 */
public class AclConfig {

    /**
     * Возвращает список ограничений для роутинга приложений
     * @return
     */
    public static Object[][] getAclConstraints() {
        //Последовательности вида путь:метод:роль1:роль2:...
        //Если вместо роли указывается правило (AUTHORIZED, ANY_USER),
        // последующие роли, для данного запроса, игнорируются.
        Object[][] constraints = new Object[][]{
                //MiscController
                {"/", GET, ANY_USER},
                //AuthController
                {"/auth/login", GET, ANY_USER},
                {"/auth/login", POST, ANY_USER},
                {"/auth/logout", POST, ANY_USER},
                //UserController
                {"/user/list", GET, USER},
                {"/user/get/**", GET, USER},
                {"/user/add", POST, ADMIN},
                {"/user/update", POST, USER},
                {"/user/delete", POST, ADMIN},
                //ClientController
                {"/client/device/add", POST, USER},
                {"/client/storage/add", POST, USER},
                {"/client/data/initial", GET, USER},
        };
        return constraints;
    }
}
