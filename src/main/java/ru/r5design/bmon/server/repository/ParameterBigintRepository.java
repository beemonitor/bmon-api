package ru.r5design.bmon.server.repository;

import org.springframework.data.repository.CrudRepository;
import ru.r5design.bmon.server.model.ParameterBigint;

public interface ParameterBigintRepository extends CrudRepository<ParameterBigint, String> {
}
