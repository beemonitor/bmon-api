package ru.r5design.bmon.server.repository;

import org.springframework.data.repository.CrudRepository;
import ru.r5design.bmon.server.model.Device;

/**
 * Created by R5 on 14.07.2016.
 */
public interface DeviceRepository extends CrudRepository<Device, Long> {
}
