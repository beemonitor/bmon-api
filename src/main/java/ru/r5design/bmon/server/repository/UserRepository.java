package ru.r5design.bmon.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.r5design.bmon.server.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByEmail(String email);
}
