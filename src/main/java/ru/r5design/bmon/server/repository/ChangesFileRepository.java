package ru.r5design.bmon.server.repository;

/**
 * Created by R5 on 03.06.2016.
 */

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.r5design.bmon.server.model.ChangesFile;

@Repository
public interface ChangesFileRepository extends CrudRepository<ChangesFile, Long> {

    @Query("SELECT c FROM ChangesFile c WHERE c.userId = ?1 AND c.startId = 0")
    public ChangesFile findInitialForUser(Long userId);
}