package ru.r5design.bmon.server.repository;

import org.springframework.data.repository.CrudRepository;
import ru.r5design.bmon.server.model.UserGroup;

public interface UserGroupRepository extends CrudRepository<UserGroup, Long> {
}

