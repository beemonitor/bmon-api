package ru.r5design.bmon.server.scheduling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.config.StoredParameters;
import ru.r5design.bmon.server.model.ChangesetFile;
import ru.r5design.bmon.server.repository.ChangesFileRepository;
import ru.r5design.bmon.server.repository.UserRepository;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Задача обновления стартовых баз с изменениями для мобильных клиентов
 */
//@Component
public class ChangesetsUpdateTask {

    //Интервал между запусками задачи
    private static final long INTERVAL = 10 * 60 * 1000;
    //Промежуток времени перед первым запуском задачи
    private static final long BEFORE_FIRST_WAIT = 1000;//10*60*1000;
    @Autowired
    GlobalConfig globalConfig;
    @Autowired
    StoredParameters storedParameters;
    /*
    @Autowired
    UserAffectedRepository affectedUsersRep;
    @Autowired
    ApiaryChangeRepository apiaryChangeRep;
    @Autowired
    LinkedUserRepository linkedUserRep;
    */
    @Autowired
    ChangesFileRepository changesFileRep;
    @Autowired
    UserRepository userRep;

    /**
     * Добавляет все изменения в файл изменений
     *
     * @param cf            Файл изменений
     * @param userId        идкентификатор пользователя, для которого ищутся изменения
     * @param startChangeId идентификатор изменения, после которого берутся изменения
     * @param endChangeId   идентификатор изменения, вплоть до которого (включительно) берутся изменения
     */
    private void addAllChanges(ChangesetFile cf, Long userId, Long startChangeId, Long endChangeId) throws IOException, SQLException {
        //Iterable<LinkedUser> changes = apiaryChangeRep.findForUserInRange(userId,startChangeId,endChangeId);
        /*
        Iterable<LinkedUser> changes2 = apiaryChangeRep.findForUserInRange();
        Iterable<LinkedUser> changes = apiaryChangeRep.findAll();
        cf.addApiaryChanges(changes);
        Iterable<LinkedUser> linkedUsers = linkedUserRep.findLinkedInRange(userId, startChangeId, endChangeId);
        cf.addLinkedUsers(linkedUsers);
        */
    }

    /**
     * Формирует или дополняет файл изменений для конкретного пользователя
     *
     * @param userId Идентификатор пользователя
     * @return имя файла с сохранёнными изменениями
     * @throws SQLException
     */
    private String saveChangesForUser(Long userId, Long startChangeId, Long endChangeId) throws SQLException {
        ChangesetFile cf = null;
        try {
            cf = new ChangesetFile(
                    globalConfig.getChangesFolder(),
                    userId.toString(), "-initial",
                    globalConfig.getMobileSqlFolder() + "changeset_init_db.db"
            );
            addAllChanges(cf, userId, startChangeId, endChangeId);
            cf.commitAndClose();
        } catch (Exception e) {
            e.printStackTrace();
            cf.revertAndClose();
            return null;
        }
        return globalConfig.getChangesPath() + cf.getFileName();
    }

    @Scheduled(initialDelay = BEFORE_FIRST_WAIT, fixedDelay = INTERVAL)
    public void buildInitialChangesets() throws Exception {
        Long lastChangeId = storedParameters.getLong("last_packed_change");
        Long currentChangeId = storedParameters.getLong("last_change");
        /*
        Iterable<UserAffected> affects = affectedUsersRep.findAffectedWithChangesFilesAfter(lastChangeId);
        for (UserAffected u : affects) {
            String changesFileName = saveChangesForUser(u.getUserId(), lastChangeId, currentChangeId);
            ChangesFile cf = u.getInitialChangesFile();
            if (cf.getPath() == null && changesFileName != null) {
                cf.setPath(changesFileName);
                cf.setEndId(currentChangeId);
                changesFileRep.save(cf);
            }
        }
*/
        storedParameters.set("last_packed_change", currentChangeId);
    }
}
