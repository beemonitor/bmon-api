package ru.r5design.bmon.server.query;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import ru.r5design.bmon.server.model.LinkedUser;

import java.util.List;

@Mapper
public interface LinkedUsersMapper {

    @Select("SELECT * FROM users_linked")
    List<LinkedUser> findAll();
}
