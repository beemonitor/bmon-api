package ru.r5design.bmon.server.dataform;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import ru.r5design.bmon.server.config.GlobalConfig;

/**
 * Обёртка для всех возвращаемых API результатов. Нужна для добавления общих атрибутов к каждому из JSON-ответов сервера
 */
@JsonPropertyOrder({"apiVersion", "response"})
public class JSONResponseWrapper<T> {

    public String apiVersion;
    public T response;

    public JSONResponseWrapper(T response, GlobalConfig config) {
        this.response = response;
        apiVersion = config.getApiVersion();
    }

    @JsonCreator
    public JSONResponseWrapper(@JsonProperty("apiVersion") String apiVersion, @JsonProperty("response") T response) {
        this.apiVersion = apiVersion;
        this.response = response;
    }

    public T getResponse() {
        return response;
    }
}
