package ru.r5design.bmon.server.dataform;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangesetJSON {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String link = null;
    @JsonIgnore
    private ChangesetType type = null;

    public ChangesetJSON() {
    }

    @JsonCreator
    public ChangesetJSON(@JsonProperty("type") String changesetType) {
        this.type = ChangesetType.getForName(changesetType);
    }

    @JsonProperty(value = "type")
    public String getTypeName() {
        if (type == null) {
            return null;
        }
        return type.getName();
    }

    public ChangesetType getType() {
        return type;
    }

    public void setType(ChangesetType type) {
        this.type = type;
    }
}
