package ru.r5design.bmon.server.dataform;

import java.util.HashMap;

public enum ChangesetType {
    EMPTY("empty"), LINK("link");

    private static HashMap<String, ChangesetType> cache = null;
    private String name;

    ChangesetType(String name) {
        this.name = name;
    }

    private static HashMap<String, ChangesetType> getCache() {
        if (cache != null) {
            return cache;
        }
        cache = new HashMap<>();
        ChangesetType[] types = ChangesetType.class.getEnumConstants();
        for (ChangesetType t : types) {
            cache.put(t.getName(), t);
        }
        return cache;
    }

    public static ChangesetType getForName(String name) {
        return getCache().get(name);
    }

    public String getName() {
        return name;
    }

}
