package ru.r5design.bmon.server.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.Map;

/**
 * Дополнительные функции для работы с запросами
 */
public class RequestHelper {

    /**
     * Производит POST-запрос с указанными параметрами
     *
     * @param client клиент, посредством которого производится запрос
     * @param params параметры запроса ("имя", "значение")
     * @return тело ответа на запрос
     */
    public static String postRequest(CloseableHttpClient client, String url, Map<String, String> params) throws IOException {
        HttpPost req = new HttpPost(url);
        LinkedList<NameValuePair> reqParams = new LinkedList<>();
        for (Map.Entry<String, String> param : params.entrySet()) {
            reqParams.add(
                    new BasicNameValuePair(param.getKey(), param.getValue())
            );
        }
        req.setEntity(new UrlEncodedFormEntity(reqParams, Charset.forName("UTF-8")));
        return client.execute(req, new BasicResponseHandler());
    }

    public static <T> T getJsonObject(CloseableHttpClient client, String url, TypeReference tr) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String body = client.execute(
                new HttpGet(url),
                new BasicResponseHandler()
        );
        return mapper.readValue(body, tr);
    }
}
