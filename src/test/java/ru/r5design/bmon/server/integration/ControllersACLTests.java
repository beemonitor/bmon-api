package ru.r5design.bmon.server.integration;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.flywaydb.test.annotation.FlywayTest;
import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.bind.annotation.RequestMethod;
import org.testng.TestException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.r5design.bmon.server.BmonApplication;
import ru.r5design.bmon.server.config.GlobalConfig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Тесты доступа к методам контроллеров исходя из роли юзера
 */
@FlywayTest
@SpringApplicationConfiguration(classes = BmonApplication.class)
@WebIntegrationTest(value = {"spring.profiles.active=test"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class})
public class ControllersACLTests extends AbstractTestNGSpringContextTests {

    @Autowired
    GlobalConfig globalConfig;

    TestsAuthenticationManager authMgr;

    //Список ссылок URLов для проверки от имени админа/юзера/анонимного пользователя
    //Подразумевается, что в adminUrls входят ссылки, недоступные юзеру
    //В свою очередь, в userUrls ходят только ссылки, недоступные анонимному пользователю
    HashMap<String, RequestMethod[]> adminUrls;
    CloseableHttpClient adminClient;
    HashMap<String, RequestMethod[]> userUrls;
    CloseableHttpClient userClient;
    HashMap<String, RequestMethod[]> anonymousUrls;
    CloseableHttpClient anonymousClient;

    String baseUrl;

    @BeforeMethod
    public void setUp() throws Exception {
        baseUrl = globalConfig.getBaseUrl();
        authMgr = TestsAuthenticationManager.getInstance(baseUrl);

        anonymousClient = authMgr.getAnonymousClient();
        anonymousUrls = new HashMap<String, RequestMethod[]>() {{
            put("/", new RequestMethod[]{GET});
            put("/auth/login", new RequestMethod[]{GET, POST});
            put("/auth/logout", new RequestMethod[]{POST});
        }};

        userClient = authMgr.getUserClient();
        userUrls = new HashMap<String, RequestMethod[]>() {{
            put("/user/list", new RequestMethod[]{GET});
            put("/user/get/1", new RequestMethod[]{GET});
            put("/user/update", new RequestMethod[]{POST});
            put("/client/device/add", new RequestMethod[]{POST});
            put("/client/storage/add", new RequestMethod[]{POST});
        }};

        adminClient = authMgr.getAdminClient();
        adminUrls = new HashMap<String, RequestMethod[]>() {{
            put("/user/add", new RequestMethod[]{POST});
        }};
    }

    /**
     * Производит запрос указанного типа, без параметров, по указанному пути.
     *
     * @param client авторизованный клиент
     * @param path   проверяемый путь
     * @param method метод запроса
     * @throws IOException
     */
    private void emptyRequest(CloseableHttpClient client, String path, RequestMethod method) throws IOException {
        HttpUriRequest request = null;
        if (method.equals(GET)) {
            request = new HttpGet(baseUrl + path);
        } else if (method.equals(POST)) {
            request = new HttpPost(baseUrl + path);
        }
        try {
            client.execute(
                    request,
                    new BasicResponseHandler()
            );
        } catch (HttpResponseException e) {
            //При недостатке параметров в POST-запросе, сервер кидает ошибку 400 (Bad Request), что приемлемо
            if (!(method.equals(POST) && e.getStatusCode() == HttpStatus.BAD_REQUEST.value())) {
                throw e;
            }
        }
    }

    /**
     * Проверяет реакцию контроллера на запрошенный с указанным методом путь
     *
     * @param clientName      имя проверяемого клиента (для вывода информации)
     * @param expectForbidden должен ли сервис запретить доступ к указанному действию, для данного клиента
     * @param client          авторизованный клиент
     * @param path            проверяемый путь
     * @param method          метод запроса
     */
    private void testRequestReaction(String clientName, boolean expectForbidden, CloseableHttpClient client, String path, RequestMethod method) throws IOException {
        try {
            try {
                emptyRequest(client, path, method);
                if (expectForbidden) {
                    throw new TestException("Method is not forbidden");
                }
            } catch (HttpResponseException e) {
                //При недостатке привилегий пользователя, сервис шлёт REDIRECT на страницу логина
                if (!(expectForbidden && (
                        e.getStatusCode() == HttpStatus.FOUND.value() || e.getStatusCode() == HttpStatus.FORBIDDEN.value()
                )
                )) {
                    throw e;
                }
            }
        } catch (Exception e) {
            System.err.println("Trouble with " + clientName + " : " + path + " : " + method);
            throw e;
        }
    }

    @Test(enabled = false)
    public void anonymousLinks() throws IOException {
        for (Map.Entry<String, RequestMethod[]> testPath : anonymousUrls.entrySet()) {
            for (RequestMethod method : testPath.getValue()) {
                //Все пользователи имеют доступ к анонимным URLам
                testRequestReaction("Anonymous", false, anonymousClient, testPath.getKey(), method);
                testRequestReaction("User", false, userClient, testPath.getKey(), method);
                testRequestReaction("Administrator", false, adminClient, testPath.getKey(), method);
            }
        }
    }

    @Test(enabled = false)
    public void userLinks() throws IOException {
        for (Map.Entry<String, RequestMethod[]> testPath : userUrls.entrySet()) {
            for (RequestMethod method : testPath.getValue()) {
                //Анонимные пользователи не имеют доступа к защищённым URLам
                //Администратор имеет доступ ко всем URLам, доступным для пользователей
                testRequestReaction("Anonymous", true, anonymousClient, testPath.getKey(), method);
                testRequestReaction("User", false, userClient, testPath.getKey(), method);
                testRequestReaction("Administrator", false, adminClient, testPath.getKey(), method);
            }
        }
    }

    @Test(enabled = false)
    public void adminLinks() throws IOException {
        for (Map.Entry<String, RequestMethod[]> testPath : adminUrls.entrySet()) {
            for (RequestMethod method : testPath.getValue()) {
                System.out.println(testPath.getKey()+" : "+method.toString());
                //Только администратор имеет доступ к своим URLам
                testRequestReaction("Anonymous", true, anonymousClient, testPath.getKey(), method);
                testRequestReaction("User", true, userClient, testPath.getKey(), method);
                testRequestReaction("Administrator", false, adminClient, testPath.getKey(), method);
            }
        }
    }

}