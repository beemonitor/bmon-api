package ru.r5design.bmon.server.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.flywaydb.test.annotation.FlywayTest;
import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.r5design.bmon.server.BmonApplication;
import ru.r5design.bmon.server.config.GlobalConfig;
import ru.r5design.bmon.server.dataform.DeviceDataJSON;
import ru.r5design.bmon.server.dataform.JSONResponseWrapper;
import ru.r5design.bmon.server.dataform.StorageDataJSON;
import ru.r5design.bmon.server.dataform.UserDataJSON;
import ru.r5design.bmon.server.model.User;

import java.io.IOException;
import java.util.HashMap;

/**
 * Тесты работы контроллера, отвечающего за функции, связанные с мобильным клиентом (/client/**)
 */
@FlywayTest
@SpringApplicationConfiguration(classes = BmonApplication.class)
@WebIntegrationTest(value = {"spring.profiles.active=test"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class})
public class ClientControllerTests extends AbstractTestNGSpringContextTests {

    CloseableHttpClient client;
    TestsAuthenticationManager authMgr;
    String clientPrefix;
    ObjectMapper mapper;
    TypeReference<JSONResponseWrapper<DeviceDataJSON>> deviceWrapperTypeRef;
    TypeReference<JSONResponseWrapper<StorageDataJSON>> storageWrapperTypeRef;
    TypeReference<JSONResponseWrapper<UserDataJSON>> userWrapperTypeRef;

    @Autowired
    GlobalConfig globalConfig;

    @BeforeMethod
    public void setUp() throws Exception {
        clientPrefix = globalConfig.getBaseUrl() + "/client";
        authMgr = TestsAuthenticationManager.getInstance(globalConfig.getBaseUrl());
        client = authMgr.getUserClient();
        mapper = new ObjectMapper();
        deviceWrapperTypeRef = new TypeReference<JSONResponseWrapper<DeviceDataJSON>>() {
        };
        storageWrapperTypeRef = new TypeReference<JSONResponseWrapper<StorageDataJSON>>() {
        };
        userWrapperTypeRef = new TypeReference<JSONResponseWrapper<UserDataJSON>>() {
        };
    }

    /**
     * Добавляет устройство в систему
     *
     * @param name имя добавляемого устройства
     * @return идентификатор добавленного устройства
     */
    Long addDevice(String name) throws IOException {
        String url = clientPrefix + "/device/add";
        HashMap params = new HashMap<String, String>() {{
            put("name", name);
        }};
        String body = RequestHelper.postRequest(client, url, params);
        JSONResponseWrapper<DeviceDataJSON> deviceObj = mapper.readValue(body, deviceWrapperTypeRef);
        return deviceObj.getResponse().deviceId;
    }

    @Test(enabled = false)
    public void deviceAdd() throws IOException {
        String firstDevice = "Android 4.2 - ALCATEL One Touch POP C9 7047D";
        String secondDevice = "iOS 9.7 - iToaster 4S Limited edition";
        Long firstDeviceId = addDevice(firstDevice);
        Long secondDeviceId = addDevice(secondDevice);
        Assert.isTrue(secondDeviceId > firstDeviceId);
    }

    /**
     * Получает данные активного пользователя исходя из параметров соединения
     *
     * @param client клиент, с заданным токеном авторизации активного пользователя
     * @return
     */
    private User getUser(CloseableHttpClient client) throws IOException {
        String url = globalConfig.getBaseUrl() + "/user/get";
        JSONResponseWrapper<UserDataJSON> userObj = RequestHelper.getJsonObject(client, url, userWrapperTypeRef);
        return userObj.getResponse().user;
    }

    /**
     * Добавляет новое локальное хранилище в систему
     *
     * @param user_id
     * @param device_id
     * @return идентификатор добавленного локального хранилища
     * @throws IOException
     */
    Long addStorage(Long user_id, Long device_id) throws IOException {
        String url = clientPrefix + "/storage/add";
        HashMap params = new HashMap<String, String>() {{
            put("device_id", device_id.toString());
        }};
        String body = RequestHelper.postRequest(client, url, params);
        JSONResponseWrapper<StorageDataJSON> storageObj = mapper.readValue(body, storageWrapperTypeRef);
        return storageObj.getResponse().storageId;
    }

    @Test(enabled = false)
    public void storageAdd() throws IOException {
        String url = clientPrefix + "/storage/add";
        Long userId = getUser(client).getId();
        Long deviceId = addDevice("Blackberry 42.0 - Test device");
        Long storageIdFirst = addStorage(userId, deviceId);
        Long storageIdSecond = addStorage(userId, deviceId);
        Assert.isTrue(storageIdSecond > storageIdFirst);
    }
}
