package ru.r5design.bmon.server.integration;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.flywaydb.test.annotation.FlywayTest;
import org.flywaydb.test.junit.FlywayTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.TestException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.r5design.bmon.server.BmonApplication;
import ru.r5design.bmon.server.config.GlobalConfig;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

/**
 * Тестирование процессов, связанных с авторизацией.
 * Проверка авторизации происходит в процессе вызовов TestAuthenticationManager.
 */
@FlywayTest
@SpringApplicationConfiguration(classes = BmonApplication.class)
@WebIntegrationTest(value = {"spring.profiles.active=test"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, FlywayTestExecutionListener.class})
public class AuthTests extends AbstractTestNGSpringContextTests {

    String internalUrl;
    String internalPostUrl;
    String logoutUrl;
    CloseableHttpClient anonymousClient;
    CloseableHttpClient authorizedClient;

    TestsAuthenticationManager authMgr;

    @Autowired
    GlobalConfig globalConfig;

    @BeforeMethod
    public void setUp() throws Exception {
        authMgr = TestsAuthenticationManager.getInstance(globalConfig.getBaseUrl());
        internalUrl = globalConfig.getBaseUrl() + "/user/list";
        internalPostUrl = globalConfig.getBaseUrl() + "/user/update";
        logoutUrl = globalConfig.getBaseUrl() + "/auth/logout";
        anonymousClient = authMgr.getAnonymousClient();
        authorizedClient = authMgr.getUserClient();
    }

    @Test(enabled = false)
    /**
     * Проверяет, запрещён ли анонимный доступ к внутреннему ресурсу.
     */
    public void accessDenied() throws IOException {
        try {
            anonymousClient.execute(
                    new HttpGet(internalUrl),
                    new BasicResponseHandler()
            );
            throw new AssertionError();
        } catch (HttpResponseException e) {
            assertEquals(e.getStatusCode(), HttpStatus.FORBIDDEN.value());
        }
    }

    @Test(enabled = false)
    /**
     * Проверяет, имеет ли авторизованный пользователь доступ к внутреннему ресурсу.
     */
    public void accessGranted() throws IOException {
        authorizedClient.execute(
                new HttpGet(internalUrl),
                new BasicResponseHandler()
        );
    }

    @Test(enabled = false, dependsOnMethods = {"accessGranted", "postGranted"})
    public void logout() throws IOException {
        try {
            authorizedClient.execute(
                    new HttpPost(logoutUrl),
                    new BasicResponseHandler()
            );
        } catch (HttpResponseException e) {
            assertEquals(e.getStatusCode(), HttpStatus.FOUND.value());
        }
        try {
            authorizedClient.execute(
                    new HttpGet(internalUrl),
                    new BasicResponseHandler()
            );
            throw new TestException("Method doesn't throw exception, but must");
        } catch (HttpResponseException e) {
            assertEquals(e.getStatusCode(), HttpStatus.FORBIDDEN.value());
        }
    }

    @Test(enabled = false)
    public void postForbidden() throws IOException {
        try {
            anonymousClient.execute(
                    new HttpPost(internalPostUrl),
                    new BasicResponseHandler()
            );
            throw new AssertionError();
        } catch (HttpResponseException e) {
            assertEquals(e.getStatusCode(), HttpStatus.FOUND.value());
        }
    }

    @Test(enabled = false)
    public void postGranted() throws IOException {
        try {
            authorizedClient.execute(
                    new HttpPost(internalPostUrl),
                    new BasicResponseHandler()
            );
        }
        catch (HttpResponseException e) {
            //При недостатке параметров в POST-запросе, сервер кидает ошибку 400 (Bad Request), что приемлемо
            if (!(e.getStatusCode()== HttpStatus.BAD_REQUEST.value())) {
                throw e;
            }
        }
    }

    //TODO
}
