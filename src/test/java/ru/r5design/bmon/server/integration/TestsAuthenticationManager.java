package ru.r5design.bmon.server.integration;

import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Класс, отвечающий за аутентификацию юзера, предшествующую интеграционным тестам
 */
public class TestsAuthenticationManager {
    private static TestsAuthenticationManager ourInstance = new TestsAuthenticationManager();

    //Не нашёл как заавтовайрить BaseConfig, поэтому костыли
    String baseUrl="";
    //Куки и параметры для просто авторизованного юзера с минимальными привилегиями
    List<Cookie> userCookies=null;
    LinkedList<NameValuePair> userReqParams = new LinkedList<NameValuePair>(){{
        add(new BasicNameValuePair("email", "hat@bmon.dev"));
        add(new BasicNameValuePair("password", "hatred"));
        add(new BasicNameValuePair("cookie", "true"));
    }};
    //Куки и параметры админа
    List<Cookie> adminCookies=null;
    LinkedList<NameValuePair> adminReqParams = new LinkedList<NameValuePair>(){{
        add(new BasicNameValuePair("email", "raptor985@ya.ru"));
        add(new BasicNameValuePair("password", "crAjaq2h"));
        add(new BasicNameValuePair("cookie", "true"));
    }};

    /**
     * Получает экземпляр для выдёргивания необходимых для авторизации Cookies
     *
     * @param baseUrl Базовый URL сервиса (Пример: "http://127.0.0.1:8080")
     * @return
     */
    public static TestsAuthenticationManager getInstance(String baseUrl) {
        if (!baseUrl.equals(ourInstance.baseUrl)) {
            ourInstance.baseUrl = baseUrl;
            ourInstance.resetCookies();
        }
        return ourInstance;
    }

    void resetCookies() {
        userCookies=null;
        adminCookies=null;
    }

    List<Cookie> getCookiesForCredentials(final List<NameValuePair> creds) {
        HttpPost req = new HttpPost(baseUrl+"/auth/login");
        req.setEntity(new UrlEncodedFormEntity(creds, Charset.forName("UTF-8")));
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpClientContext context = HttpClientContext.create();
        CloseableHttpResponse response = null;
        try {
            response = httpclient.execute(req, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Cookie> res = new ArrayList<>();
        try {
            CookieStore cookieStore = context.getCookieStore();
            List<Cookie> cookies = cookieStore.getCookies();
            for (Cookie c : cookies) {
                res.add(c);
            }
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    List<Cookie> getUserCookies() {
        if (userCookies==null) {
            userCookies = getCookiesForCredentials(userReqParams);
        }
        return userCookies;
    }

    List<Cookie> getAdminCookies() {
        if (adminCookies==null) {
            adminCookies = getCookiesForCredentials(adminReqParams);
        }
        return adminCookies;
    }

    public CloseableHttpClient getUserClient() {
        CookieStore cs = new BasicCookieStore();
        for (Cookie c : getUserCookies()) {
            cs.addCookie(c);
        }
        return HttpClientBuilder.create().setDefaultCookieStore(cs).build();
    }

    public CloseableHttpClient getAdminClient() {
        CookieStore cs = new BasicCookieStore();
        for (Cookie c : getAdminCookies()) {
            cs.addCookie(c);
        }
        return HttpClientBuilder.create().setDefaultCookieStore(cs).build();
    }

    public CloseableHttpClient getAnonymousClient() {
        return HttpClients.createDefault();
    }
}
